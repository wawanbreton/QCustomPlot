/***************************************************************************
**                                                                        **
**  QCustomPlot, an easy to use, modern plotting widget for Qt            **
**  Copyright (C) 2011, 2012, 2013, 2014 Emanuel Eichhammer               **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Erwan Mathieu                                        **
**  Website/Contact: http://www.qcustomplot.com/                          **
**             Date: 10.03.14                                             **
**          Version: 1.2.0-beta                                           **
****************************************************************************/

#include "interaction.h"

#include "core.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////// QCPAbstractInteraction
////////////////////////////////////////////////////////////////////////////////////////////////////

/*! \class QCPAbstractInteraction
  \brief Base class for user interactions

  An interaction is basically the user pressing the mouse over the plot, moving it around,
  and releasing the button. According to the implementation, the interaction may select a curve,
  zoom in/out, translate the axis, ...

  Standard interactions can be activated/deactivated using \ref QCustomPlot::setInteractions. They
  will use the most common buttons, and no keyboard modifiers, to suit usual needs. If you want to
  customize an interaction, retrieve it using \ref QCustomPlot::interaction and call its specific
  methods.

  Each interaction can be individually activated or deactived by calling \ref setActive. When an
  interaction is inactive, it is ignored by the parent plot which does not call the event methods.
  This is useful to activate/deactive interactions without destroying/recreating them.

  This class is an abstract placeholder for all interactions. To add you own custom interactions,
  override it and call \ref QCustomPlot::addInteraction to make it fully active.

  When using many interactions at the same time, you should take care that they don't activate
  together. For example, if you set a drag range and a zoom rect interaction both on the left mouse
  button, this is going to do weird things, because each will catch the mouse events and to their
  job. However, you can use a select and a zoom rect interaction on the left button, because
  selection works on a single point click and zoom rect uses a mouse mouse : the select will
  activate only if the cursor didn't move between click and release, and the zoom will activate only
  if the cursor relevantly moved between click and release. To avoid bad cases, you can also use
  keyboard modifiers.
*/

/*!
  Creates a new QCPAbstractInteraction. This constructor is to be used by child classes only.
*/
QCPAbstractInteraction::QCPAbstractInteraction(QCustomPlot *parentPlot) :
  QObject(parentPlot),
  mParentPlot(parentPlot),
  mActive(true)
{
}

QCPAbstractInteraction::~QCPAbstractInteraction()
{
}

/*!
  Converts the Qt::MouseButton retrieved on the QMouseEvent to a QCP MouseButton enumeration.
  This is required to ease the configuration of interactions by including the wheel into the
  enumeration, as if it were just two standard buttons.
*/
QCPAbstractInteraction::MouseButton QCPAbstractInteraction::toMouseButton(Qt::MouseButton button)
{
  switch (button)
  {
    case Qt::LeftButton:
      return mbLeft;
    case Qt::RightButton:
      return mbRight;
    case Qt::MiddleButton:
      return mbMiddle;
    case Qt::XButton1:
      return mbXButton1;
    case Qt::XButton2:
      return mbXButton2;
    default:
      return mbNone;
  }
}
