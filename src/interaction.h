/***************************************************************************
**                                                                        **
**  QCustomPlot, an easy to use, modern plotting widget for Qt            **
**  Copyright (C) 2011, 2012, 2013, 2014 Emanuel Eichhammer               **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Erwan Mathieu                                        **
**  Website/Contact: http://www.qcustomplot.com/                          **
**             Date: 07.03.14                                             **
**          Version: 1.2.0-beta                                           **
****************************************************************************/

#ifndef QCP_INTERACTION_H
#define QCP_INTERACTION_H

#include "global.h"
#include "layer.h"
#include "axis.h"

class QCustomPlot;

class QCP_LIB_DECL QCPAbstractInteraction : public QObject
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(bool active READ active WRITE setActive)
  /// \endcond
public:
  /*!
    Defines the mouse buttons which may be pressed/released/triggered
  */
  enum MouseButton { mbNone       ///< No button
                     ,mbLeft      ///< The mouse standard left button
                     ,mbRight     ///< The mouse standard right button
                     ,mbMiddle    ///< The mouse middle button, usually triggered by pressing the wheel
                     ,mbWheelUp   ///< Pseudo-button, triggered when the wheel has been moved up
                     ,mbWheelDown ///< Pseudo-button, triggered when the wheel has been moved down
                     ,mbXButton1  ///< First optional mouse button, usually located on the side
                     ,mbXButton2  ///< Seconcd optional mouse button, usually located on the side
                   };

  QCPAbstractInteraction(QCustomPlot *parentPlot);
  virtual ~QCPAbstractInteraction();
  
  // getters:
  QCustomPlot *parentPlot() const { return mParentPlot; }
  bool active() const { return mActive; }

  // setters:
  void setActive(bool active) { mActive = active; }

  // introduced virtual method:
  virtual void mouseDoubleClickEvent(QMouseEvent *event) { Q_UNUSED(event); }
  virtual void mousePressEvent(QMouseEvent *event) { Q_UNUSED(event); }
  virtual void mouseMoveEvent(QMouseEvent *event) { Q_UNUSED(event); }
  virtual void mouseReleaseEvent(QMouseEvent *event) { Q_UNUSED(event); }
  virtual void mouseWheelEvent(QWheelEvent *event) { Q_UNUSED(event); }

  // static methods:
  MouseButton toMouseButton(Qt::MouseButton button);
  
protected:
  // property members:
  QCustomPlot *mParentPlot;
  bool mActive;
  
private:
  Q_DISABLE_COPY(QCPAbstractInteraction)
};

#endif // QCP_INTERACTION_H
