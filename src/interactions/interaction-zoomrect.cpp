/***************************************************************************
**                                                                        **
**  QCustomPlot, an easy to use, modern plotting widget for Qt            **
**  Copyright (C) 2011, 2012, 2013, 2014 Emanuel Eichhammer               **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Erwan Mathieu                                        **
**  Website/Contact: http://www.qcustomplot.com/                          **
**             Date: 12.03.14                                             **
**          Version: 1.2.0-beta                                           **
****************************************************************************/

#include "interaction-zoomrect.h"

#include "../axis.h"
#include "../core.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////// QCPInteractionZoomRect
////////////////////////////////////////////////////////////////////////////////////////////////////

/*! \class QCPInteractionZoomRect
  \brief Interaction for zooming on a specific area by drawing a rectangle around it.

  This interaction works in 3 steps : the user cliks on the plot, then moves the mouse around which
  draws a selection rectangle (a QRubberBand), then the mouse button is released. At this time,
  the drawn rectangle is considered to be the new axis ranges. This allows to move quickly from a
  graph containing many data to an area containing interesting information. It also offers the
  ability to zoom separately on the X and Y axis, so that the displayed data may be more relevant.

  To make this interaction fully working, you need to call the \ref addAxis method as many times as
  required to insert all the axis you want to be zoomed.

  This interaction only allows for zoom in. If you need to zoom out, use it in parallel with an
  other interaction like zoom range, or add an external button to set the axes ranges manually.
*/

/* start documentation of inline functions */


/* end documentation of inline functions */

QCPInteractionZoomRect::QCPInteractionZoomRect(QCustomPlot *parentPlot) :
  QCPAbstractInteractionOneButton(parentPlot),
  mAxis(),
  mMousePressPos(),
  mRubberBand(new QRubberBand(QRubberBand::Rectangle, parentPlot))
{
  mRubberBand->hide();
}

QCPInteractionZoomRect::~QCPInteractionZoomRect()
{
}

void QCPInteractionZoomRect::buttonActivated(QMouseEvent *event)
{
  if (!mAxis.isEmpty())
  {
    mMousePressPos = event->pos();
    mRubberBand->setGeometry(QRect(mMousePressPos, QSize(0, 0)));
    mRubberBand->show();
  }
}

void QCPInteractionZoomRect::mouseMoveEvent(QMouseEvent *event)
{
  if (mRubberBand->isVisible())
    mRubberBand->setGeometry(QRect(mMousePressPos, event->pos()).normalized());

  QCPAbstractInteractionOneButton::mouseMoveEvent(event);
}

void QCPInteractionZoomRect::buttonDeactivated(QMouseEvent *event)
{
  Q_UNUSED(event)
  bool ranged = false;
  if (mRubberBand->isVisible())
  {
    mRubberBand->hide();
    QRect rect = mRubberBand->geometry();
    if (rect.width() > 10 && rect.height() > 10) // protection against involoutary moves
    {
      foreach (QPointer<QCPAxis> axis, mAxis)
      {
        if (!axis.isNull())
        {
          double lower;
          double upper;

          if (axis->axisType() == QCPAxis::atLeft || axis->axisType() == QCPAxis::atRight)
          {
            // vertical axis, take the rect height
            lower = axis->pixelToCoord(rect.bottom());
            upper = axis->pixelToCoord(rect.top());
          }
          else
          {
            // horizontal axis, take the rect width
            lower = axis->pixelToCoord(rect.left());
            upper = axis->pixelToCoord(rect.right());
          }

          axis->setRange(lower, upper);
          ranged = true;
        }
      }
    }
  }

  if (ranged)
    mParentPlot->replot();
}
