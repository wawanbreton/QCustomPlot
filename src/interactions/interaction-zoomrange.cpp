/***************************************************************************
**                                                                        **
**  QCustomPlot, an easy to use, modern plotting widget for Qt            **
**  Copyright (C) 2011, 2012, 2013, 2014 Emanuel Eichhammer               **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Erwan Mathieu                                        **
**  Website/Contact: http://www.qcustomplot.com/                          **
**             Date: 10.03.14                                             **
**          Version: 1.2.0-beta                                           **
****************************************************************************/

#include "interaction-zoomrange.h"

#include "../axis.h"
#include "../core.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////// QCPInteractionZoomRange
////////////////////////////////////////////////////////////////////////////////////////////////////

/*! \class QCPInteractionZoomRange
  \brief Interaction for mouse zoom in/out using the wheel or buttons.

  The axis to be zoomed in/out are to be set using the \ref addAxis method. If no axis is set, the
  interaction will do nothing. If many axis are set, they will all be zoomed when the interacton is
  triggered. The center of the scaling operation is the current cursor position inside the axis
  rect. The strength of the zoom can be controlled for each axis when adding them. However, using
  different factors for each axis may be disturbing unless you have a very specific application.
*/

/* start documentation of inline functions */

/*! \fn void addAxis(QCPAxis *axis, double factor)

  Adds an axis to be zoomed by the user with the mouse. Once the axis has been added, its range will
  be scaled each time the interaction is triggered. By default, the interactions contains no axis,
  making it useless.

  You can add as many axis as wanted, they will all be treated independantly.

  The \a factor indicates how strong the zoom is. The default value makes a natural zoom, but you can
  increase it if your application requires that you move very quickly from a large view to a small
  view. Or you can decrease it if you need to set a very specific zoom level. The factor should be
  lesser than 1, or the zoom will be inverted.
*/

/* end documentation of inline functions */

QCPInteractionZoomRange::QCPInteractionZoomRange(QCustomPlot *parentPlot) :
  QCPAbstractInteraction(parentPlot),
  mButtonZoomIn(mbWheelUp),
  mButtonZoomOut(mbWheelDown),
  mAxis(),
  mMousePressPos()
{
}

QCPInteractionZoomRange::~QCPInteractionZoomRange()
{
}

/*!
  Function provided for convience : it sets the zoom factor for each registered axis, which is the
  most common case.

  The \a factor indicates how strong the zoom is. The default value makes a natural zoom, but you can
  increase it if your application requires that you move very quickly from a large view to a small
  view. Or you can decrease it if you need to set a very specific zoom level. The factor should be
  lesser than 1, or the zoom will be inverted.
*/
void QCPInteractionZoomRange::setFactor(double factor)
{
  QMutableListIterator<QPair<QPointer<QCPAxis>, double> > iterator(mAxis);
  while (iterator.hasNext())
  {
    iterator.next().second = factor;
  }
}

void QCPInteractionZoomRange::mousePressEvent(QMouseEvent *event)
{
  MouseButton button = toMouseButton(event->button());
  if (button == mButtonZoomIn || button == mButtonZoomOut)
  {
    mMousePressPos = event->pos();
  }
}

void QCPInteractionZoomRange::mouseReleaseEvent(QMouseEvent *event)
{
  MouseButton button = toMouseButton(event->button());
  if (button == mButtonZoomIn || button == mButtonZoomOut)
  {
    if ((mMousePressPos - event->pos()).manhattanLength() < 5) // determine whether it was a click operation
    {
      doZoom(event->pos(), button == mButtonZoomIn ? 1 : -1);
    }
  }
}

/*! \internal
 \brief Method called when the mouse wheel has been rotated up or down.

  Note, that event->delta() is usually +/-120 for single rotation steps. However, if the mouse
  wheel is turned rapidly, many steps may bunch up to one event, so the event->delta() may then be
  multiples of 120. This is taken into account here, by calculating \a wheelSteps and using it as
  exponent of the range zoom factor. This takes care of the wheel direction automatically, by
  inverting the factor, when the wheel step is negative (f^-1 = 1/f).
 */
void QCPInteractionZoomRange::mouseWheelEvent(QWheelEvent *event)
{
  double wheelDelta = event->delta() / 120.0; // a single step delta is +/-120 usually
  double wise = 0;

  if (wheelDelta > 0)
  {
    if (mButtonZoomIn == mbWheelUp)
      wise = 1;
    else if (mButtonZoomOut == mbWheelUp)
      wise = -1;
  }
  else
  {
    if (mButtonZoomIn == mbWheelDown)
      wise = -1;
    else if (mButtonZoomOut == mbWheelDown)
      wise = 1;
  }

  if (!qFuzzyIsNull(wise))
    doZoom(event->pos(), wheelDelta * wise);
}

/*! \internal
 \brief Actually compute the range zoom

  This method computes and applies the new ranges for each registered axis, according to the given
  zoom parameters.

  The \a pos is the current mouse position, used to zoom on a specific zone on the graph.
  The \a steps indicates how strong the zoom should be made. It may be negative to indicate
  a zoom out.
 */
void QCPInteractionZoomRange::doZoom(const QPoint &pos, double steps)
{
  bool scaled = false;
  for (int i=0 ; i<mAxis.count() ; i++)
  {
    QPointer<QCPAxis> axis = mAxis[i].first;
    if (!axis.isNull())
    {
      int mousePos;

      if (axis->axisType() == QCPAxis::atLeft || axis->axisType() == QCPAxis::atRight)
      {
        // vertical axis, take the y position of the mouse
        mousePos = pos.y();
      }
      else
      {
        // horizontal axis, take the x position of the mouse
        mousePos = pos.x();
      }

      axis->scaleRange(pow(mAxis[i].second, steps), axis->pixelToCoord(mousePos));
      scaled = true;
    }
  }

  if (scaled)
    parentPlot()->replot();
}
