/***************************************************************************
**                                                                        **
**  QCustomPlot, an easy to use, modern plotting widget for Qt            **
**  Copyright (C) 2011, 2012, 2013, 2014 Emanuel Eichhammer               **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Erwan Mathieu                                        **
**  Website/Contact: http://www.qcustomplot.com/                          **
**             Date: 10.03.14                                             **
**          Version: 1.2.0-beta                                           **
****************************************************************************/

#ifndef QCP_INTERACTION_ZOOMRANGE_H
#define QCP_INTERACTION_ZOOMRANGE_H

#include "../interaction.h"

class QCPAxis;


class QCP_LIB_DECL QCPInteractionZoomRange : public QCPAbstractInteraction
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QCPAbstractInteraction::MouseButton buttonZoomIn READ buttonZoomIn WRITE setButtonZoomIn)
  Q_PROPERTY(QCPAbstractInteraction::MouseButton buttonZoomOut READ buttonZoomOut WRITE setButtonZoomOut)
  /// \endcond
public:
  QCPInteractionZoomRange(QCustomPlot *parentPlot);
  virtual ~QCPInteractionZoomRange();

  // getters:
  MouseButton buttonZoomIn() const { return mButtonZoomIn; }
  MouseButton buttonZoomOut() const { return mButtonZoomOut; }
  int axisCount() const { return mAxis.count(); }
  QCPAxis *axis(int index) const { return mAxis[index].first; }
  double factor(int index) const { return mAxis[index].second; }

  // setters:
  void setButtonZoomIn(MouseButton button) { mButtonZoomIn = button; }
  void setButtonZoomOut(MouseButton button) { mButtonZoomOut = button; }
  void setFactor(double factor);
  void clearAxis() { mAxis.clear(); }
  void addAxis(QCPAxis *axis, double factor=0.85) { mAxis << qMakePair(QPointer<QCPAxis>(axis), factor); }

  // reimplemented virtual method:
  virtual void mousePressEvent(QMouseEvent *event);
  virtual void mouseReleaseEvent(QMouseEvent *event);
  virtual void mouseWheelEvent(QWheelEvent *event);
  
protected:
  // property members
  MouseButton mButtonZoomIn;
  MouseButton mButtonZoomOut;
  QList<QPair<QPointer<QCPAxis>, double> > mAxis;

  // non-property members
  QPoint mMousePressPos;
  QCP::AntialiasedElements mAADragBackup, mNotAADragBackup;

  // non-virtual methods:
  void doZoom(const QPoint &pos, double steps);
  
private:
  Q_DISABLE_COPY(QCPInteractionZoomRange)
};

#endif // QCP_INTERACTION_ZOOMRANGE_H
