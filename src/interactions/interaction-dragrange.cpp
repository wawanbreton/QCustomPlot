/***************************************************************************
**                                                                        **
**  QCustomPlot, an easy to use, modern plotting widget for Qt            **
**  Copyright (C) 2011, 2012, 2013, 2014 Emanuel Eichhammer               **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Erwan Mathieu                                        **
**  Website/Contact: http://www.qcustomplot.com/                          **
**             Date: 12.03.14                                             **
**          Version: 1.2.0-beta                                           **
****************************************************************************/

#include "interaction-dragrange.h"

#include "../axis.h"
#include "../core.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////// QCPInteractionDragRange
////////////////////////////////////////////////////////////////////////////////////////////////////

/*! \class QCPInteractionDragRange
  \brief Interaction to drag the plot ranges with the mouse

  This interaction works very naturally : the user presses a mouse button over the plot, moves the
  cursor around, which moves the axis ranges accordingly, and it stops when the mouse button
  is released. You can also set keyboard modifiers to trigger the drag only when some buttons are
  pressed.

  To fully activate the interaction, you need to set the dragged axis by calling \ref addAxis. The
  standard case is to drag the first X and Y axis, but you can decide to drag only the X axis if you
  have a fixed Y range, or drag the secondary X and Y axis at the same time.
*/

QCPInteractionDragRange::QCPInteractionDragRange(QCustomPlot *parentPlot) :
  QCPAbstractInteractionOneButton(parentPlot),
  mAxis(),
  mNoAntialiasingOnDrag(false),
  mMousePressPos(),
  mRangesStart()
{
}

QCPInteractionDragRange::~QCPInteractionDragRange()
{
}

/*!
  Sets whether antialiasing is disabled for this QCustomPlot while the user is dragging axes
  ranges. If many objects, especially plottables, are drawn antialiased, this greatly improves
  performance during dragging. Thus it creates a more responsive user experience. As soon as the
  user stops dragging, the last replot is done with normal antialiasing, to restore high image
  quality.
*/
void QCPInteractionDragRange::setNoAntialiasingOnDrag(bool enabled)
{
  mNoAntialiasingOnDrag = enabled;
}

void QCPInteractionDragRange::buttonActivated(QMouseEvent *event)
{
  if (!mAxis.isEmpty())
  {
    mMousePressPos = event->pos();

    // initialize antialiasing backup in case we start dragging:
    if (mNoAntialiasingOnDrag)
    {
      mAADragBackup = mParentPlot->antialiasedElements();
      mNotAADragBackup = mParentPlot->notAntialiasedElements();
    }

    mRangesStart.clear();
    foreach (QPointer<QCPAxis> axis, mAxis)
    {
      if (!axis.isNull())
      {
        mRangesStart << axis->range();
      }
    }
  }
}

void QCPInteractionDragRange::mouseMoveEvent(QMouseEvent *event)
{
  bool ranged = false;

  if (!mMousePressPos.isNull())
  {
    int i=0;
    foreach (QPointer<QCPAxis> axis, mAxis)
    {
      if (!axis.isNull())
      {
        QCPRange rangeStart = mRangesStart[i++];
        int dragStart, mousePos;
        if (axis->axisType() == QCPAxis::atLeft || axis->axisType() == QCPAxis::atRight)
        {
          // vertical axis, take the y position of the mouse
          dragStart = mMousePressPos.y();
          mousePos = event->pos().y();
        }
        else
        {
          // horizontal axis, take the x position of the mouse
          dragStart = mMousePressPos.x();
          mousePos = event->pos().x();
        }

        if (axis->scaleType() == QCPAxis::stLinear)
        {
          double diff = axis->pixelToCoord(dragStart) - axis->pixelToCoord(mousePos);
          axis->setRange(rangeStart.lower + diff, rangeStart.upper + diff);
          ranged = true;
        }
        else if (axis->scaleType() == QCPAxis::stLogarithmic)
        {
          double diff = axis->pixelToCoord(dragStart) / axis->pixelToCoord(mousePos);
          axis->setRange(rangeStart.lower * diff, rangeStart.upper * diff);
          ranged = true;
        }
      }
    }
  }

  if (ranged)
  {
    if (mNoAntialiasingOnDrag)
      mParentPlot->setNotAntialiasedElements(QCP::aeAll);
    mParentPlot->replot();
  }

  QCPAbstractInteractionOneButton::mouseMoveEvent(event);
}

void QCPInteractionDragRange::buttonDeactivated(QMouseEvent *event)
{
  Q_UNUSED(event)
  if (!mMousePressPos.isNull())
  {
    mMousePressPos = QPoint();
    if (mNoAntialiasingOnDrag)
    {
      mParentPlot->setAntialiasedElements(mAADragBackup);
      mParentPlot->setNotAntialiasedElements(mNotAADragBackup);
      mParentPlot->replot();
    }
  }
}
