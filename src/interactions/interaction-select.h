/***************************************************************************
**                                                                        **
**  QCustomPlot, an easy to use, modern plotting widget for Qt            **
**  Copyright (C) 2011, 2012, 2013, 2014 Emanuel Eichhammer               **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Erwan Mathieu                                        **
**  Website/Contact: http://www.qcustomplot.com/                          **
**             Date: 12.03.14                                             **
**          Version: 1.2.0-beta                                           **
****************************************************************************/

#ifndef QCP_INTERACTION_SELECT_H
#define QCP_INTERACTION_SELECT_H

#include "interaction-onebutton.h"


class QCP_LIB_DECL QCPInteractionSelect : public QCPAbstractInteractionOneButton
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(Qt::KeyboardModifier multiSelectModifier READ multiSelectModifier WRITE setMultiSelectModifier)
  /// \endcond
public:
  QCPInteractionSelect(QCustomPlot *parentPlot);
  virtual ~QCPInteractionSelect();

  // getters:
  Qt::KeyboardModifier multiSelectModifier() const { return mMultiSelectModifier; }
  QCP::Interactions interactions() const { return mInteractions; }

  // setters:
  void setMultiSelectModifier(Qt::KeyboardModifier modifier);
  void setInteractions(QCP::Interactions interactions) { mInteractions = interactions; }

  // reimplemented virtual method:
  virtual void buttonActivated(QMouseEvent *event);
  virtual void buttonDeactivated(QMouseEvent *event);

signals:
  void selectionChanged();
  
protected:
  // property members
  Qt::KeyboardModifier mMultiSelectModifier;
  QCP::Interactions mInteractions;

  // non-property members
  QPoint mMousePressPos;

private:
  Q_DISABLE_COPY(QCPInteractionSelect)
};

#endif // QCP_INTERACTION_SELECT_H
