/***************************************************************************
**                                                                        **
**  QCustomPlot, an easy to use, modern plotting widget for Qt            **
**  Copyright (C) 2011, 2012, 2013, 2014 Emanuel Eichhammer               **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Erwan Mathieu                                        **
**  Website/Contact: http://www.qcustomplot.com/                          **
**             Date: 21.03.14                                             **
**          Version: 1.2.0-beta                                           **
****************************************************************************/

#include "interaction-onebutton.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////// QCPAbstractInteractionOneButton
////////////////////////////////////////////////////////////////////////////////////////////////////

/*! \class QCPAbstractInteractionOneButton
  \brief Base class for interactions which are based on a single mouse button

  This class is provided for convenience, because many interactions work by using a single mouse
  button, and keyboard modifiers. The button and the modifiers can be customized easily, so that the
  final application suits the user at best.

  The interaction starts when the mouse button is pressed, and only if the pressed modifiers match
  the set modifiers at this very moment. It lasts until the mouse button is released, even if the
  keyboard modifiers are released in the meantime. This is the most natural way to do things.
*/

/* start documentation of inline functions */

/*! \fn virtual void buttonActivated(QMouseEvent *event)

  Virtual method called when the mouse button is pressed and the pressed keyboard modifiers match
  the set modifiers. The real interaction job should start there.
*/

/*! \fn virtual void buttonDeactivated(QMouseEvent *event)

  Virtual method called when the mouse button is released and the interaction is currently active.
  The real interaction job should stop here.
*/

/* end documentation of inline functions */

QCPAbstractInteractionOneButton::QCPAbstractInteractionOneButton(QCustomPlot *parentPlot) :
  QCPAbstractInteraction(parentPlot),
  mButton(mbLeft),
  mModifiers(Qt::NoModifier),
  mActive(false)
{
}

QCPAbstractInteractionOneButton::~QCPAbstractInteractionOneButton()
{
}

void QCPAbstractInteractionOneButton::mousePressEvent(QMouseEvent *event)
{
  if (toMouseButton(event->button()) == mButton && event->modifiers() == mModifiers)
  {
    mActive = true;
    buttonActivated(event);
  }
}

void QCPAbstractInteractionOneButton::mouseReleaseEvent(QMouseEvent *event)
{
  if (mActive && toMouseButton(event->button()) == mButton)
  {
    mActive = false;
    buttonDeactivated(event);
  }
}
