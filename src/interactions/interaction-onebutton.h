/***************************************************************************
**                                                                        **
**  QCustomPlot, an easy to use, modern plotting widget for Qt            **
**  Copyright (C) 2011, 2012, 2013, 2014 Emanuel Eichhammer               **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Erwan Mathieu                                        **
**  Website/Contact: http://www.qcustomplot.com/                          **
**             Date: 21.03.14                                             **
**          Version: 1.2.0-beta                                           **
****************************************************************************/

#ifndef QCP_ABSTRACTINTERACTION_ONEBUTTON
#define QCP_ABSTRACTINTERACTION_ONEBUTTON

#include "../interaction.h"


class QCP_LIB_DECL QCPAbstractInteractionOneButton : public QCPAbstractInteraction
{
  Q_OBJECT
  /// \cond INCLUDE_QPROPERTIES
  Q_PROPERTY(QCPAbstractInteraction::MouseButton button READ button WRITE setButton)
  Q_PROPERTY(Qt::KeyboardModifiers modifiers READ modifiers WRITE setModifiers)
  /// \endcond
public:
  QCPAbstractInteractionOneButton(QCustomPlot *parentPlot);
  virtual ~QCPAbstractInteractionOneButton();

  // getters:
  MouseButton button() const { return mButton; }
  Qt::KeyboardModifiers modifiers() { return mModifiers; }

  // setters:
  void setButton(MouseButton button) { mButton = button; }
  void setModifiers(Qt::KeyboardModifiers modifiers) { mModifiers = modifiers; }

protected:
  // property members:
  MouseButton mButton;
  Qt::KeyboardModifiers mModifiers;

  // non-property members
  bool mActive;

  // reimplemented virtual methods:
  void mousePressEvent(QMouseEvent *event);
  void mouseReleaseEvent(QMouseEvent *event);

  // introduced virtual methods:
  virtual void buttonActivated(QMouseEvent *event) = 0;
  virtual void buttonDeactivated(QMouseEvent *event) = 0;

  // non-virtual methods:
  void updateActive(QMouseEvent *event);

private:
  Q_DISABLE_COPY(QCPAbstractInteractionOneButton)
};

#endif // QCP_ABSTRACTINTERACTION_ONEBUTTON
