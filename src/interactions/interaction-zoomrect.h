/***************************************************************************
**                                                                        **
**  QCustomPlot, an easy to use, modern plotting widget for Qt            **
**  Copyright (C) 2011, 2012, 2013, 2014 Emanuel Eichhammer               **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Erwan Mathieu                                        **
**  Website/Contact: http://www.qcustomplot.com/                          **
**             Date: 12.03.14                                             **
**          Version: 1.2.0-beta                                           **
****************************************************************************/

#ifndef QCP_INTERACTION_ZOOMRECT_H
#define QCP_INTERACTION_ZOOMRECT_H

#include "interaction-onebutton.h"

class QRubberBand;
class QCPAxis;


class QCP_LIB_DECL QCPInteractionZoomRect : public QCPAbstractInteractionOneButton
{
  Q_OBJECT
public:
  QCPInteractionZoomRect(QCustomPlot *parentPlot);
  virtual ~QCPInteractionZoomRect();

  // getters:
  int axisCount() const { return mAxis.count(); }
  QCPAxis *axis(int index) const { return mAxis[index]; }

  // setters:
  void clearAxis() { mAxis.clear(); }
  void addAxis(QCPAxis *axis) { mAxis << axis; }

  // reimplemented virtual method:
  virtual void buttonActivated(QMouseEvent *event);
  virtual void mouseMoveEvent(QMouseEvent *event);
  virtual void buttonDeactivated(QMouseEvent *event);
  
protected:
  // property members
  QList<QPointer<QCPAxis> > mAxis;

  // non-property members
  QPoint mMousePressPos;
  QRubberBand *mRubberBand;
  
private:
  Q_DISABLE_COPY(QCPInteractionZoomRect)
};

#endif // QCP_INTERACTION_ZOOMRECT_H
