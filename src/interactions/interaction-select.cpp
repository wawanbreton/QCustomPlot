/***************************************************************************
**                                                                        **
**  QCustomPlot, an easy to use, modern plotting widget for Qt            **
**  Copyright (C) 2011, 2012, 2013, 2014 Emanuel Eichhammer               **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Erwan Mathieu                                        **
**  Website/Contact: http://www.qcustomplot.com/                          **
**             Date: 12.03.14                                             **
**          Version: 1.2.0-beta                                           **
****************************************************************************/

#include "interaction-select.h"

#include "../axis.h"
#include "../core.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////// QCPInteractionSelect
////////////////////////////////////////////////////////////////////////////////////////////////////

/*! \class QCPInteractionSelect
  \brief Interaction which lets the user select one or more elements of the plot

  This interaction is quite simple as it only manages the user mouse click and calls the
  \ref selectEvent or \ref deselectEvent of the appropriate elements. When the selection changes in
  any way, the \ref selectionChanged signal is emitted.

  The \ref setInteractions methods lets you customise which elements of the plot are to be selected,
  and whether many elements may be selected at the same time.
*/

/* start documentation of inline functions */

/*! \fn void setInteractions(QCP::Interactions interactions)

  This methods allows you to configure which elements of the plot may be selected through this
  interaction. You can combine whichever elements you want using the Interactions flags. Of course,
  only the iSelect* values of the enumeration are considered.

  You can also set the iMultiSelect flag to indicate that the user may select many elements
  simultaneously.
*/

/* end documentation of inline functions */

QCPInteractionSelect::QCPInteractionSelect(QCustomPlot *parentPlot) :
  QCPAbstractInteractionOneButton(parentPlot),
  mMultiSelectModifier(Qt::ControlModifier),
  mInteractions(0),
  mMousePressPos()
{
}

QCPInteractionSelect::~QCPInteractionSelect()
{
}

/*!
  Sets the keyboard modifier that will be recognized as multi-select-modifier.

  If \ref QCP::iMultiSelect is specified in \ref setInteractions, the user may select multiple objects
  by clicking on them one after the other while holding down \a modifier.

  By default the multi-select-modifier is set to Qt::ControlModifier.
*/
void QCPInteractionSelect::setMultiSelectModifier(Qt::KeyboardModifier modifier)
{
  mMultiSelectModifier = modifier;
}

void QCPInteractionSelect::buttonActivated(QMouseEvent *event)
{
  mMousePressPos = event->pos();
}

void QCPInteractionSelect::buttonDeactivated(QMouseEvent *event)
{
  bool selectionStateChanged = false;

  if ((mMousePressPos - event->pos()).manhattanLength() < 5) // determine whether it was a click operation
  {
    QVariant details;
    QCPLayerable *clickedLayerable = mParentPlot->layerableAt(event->pos(), true, &details);
    bool additive = mInteractions.testFlag(QCP::iMultiSelect) && event->modifiers().testFlag(mMultiSelectModifier);
    // deselect all other layerables if not additive selection:
    if (!additive)
    {
      for (int i=0 ; i<mParentPlot->layerCount() ; i++)
      {
        QCPLayer *layer = mParentPlot->layer(i);

        foreach (QCPLayerable *layerable, layer->children())
        {
          if (layerable != clickedLayerable && mInteractions.testFlag(layerable->selectionCategory()))
          {
            bool selChanged = false;
            layerable->deselectEvent(&selChanged);
            selectionStateChanged |= selChanged;
          }
        }
      }
    }
    if (clickedLayerable && mInteractions.testFlag(clickedLayerable->selectionCategory()))
    {
      // a layerable was actually clicked, call its selectEvent:
      bool selChanged = false;
      clickedLayerable->selectEvent(event, additive, details, &selChanged);
      selectionStateChanged |= selChanged;
    }
  }

  if (selectionStateChanged)
  {
    emit selectionChanged();
    mParentPlot->replot();
  }
}
